# Cryptoarbiter test. Deploy on server Ubuntu 16

## INSTALL PYTHON & PIP

`sudo add-apt-repository ppa:jonathonf/python-3.6`  

> if it gives  ***add-apt-repository: command not found***   than use: `sudo apt-get install software-properties-common`

**Put each command separatly, one by one**
```
sudo apt update
sudo apt install python3.6
sudo apt install python3.6-dev
sudo apt install python3.6-venv
wget https://bootstrap.pypa.io/get-pip.py
sudo python3.6 get-pip.py
sudo ln -s /usr/bin/python3.6 /usr/local/bin/python3
sudo ln -s /usr/local/bin/pip /usr/local/bin/pip3
sudo ln -s /usr/bin/python3.6 /usr/local/bin/python
```



## Install cryptoarbiter

```
apt-get install git-core
git clone https://gitlab.com/netfactory/cryptoarbiter.git
cd ~/cryptoarbiter; . decommenter.sh
apt install memcached
```


## Creating virtualenv using Python 3.6

```
sudo pip install virtualenv
virtualenv -p /usr/bin/python3.6 ~/cryptoarbiter/venv
cd ~/cryptoarbiter; . venv/bin/activate
pip install -r requirements.txt
deactivate
```


## Install & config supervisor


```
apt-get install supervisor
mkdir /var/log/cryptoarbiter
mkdir /var/log/cryptoarbiter/websocket
mkdir /var/log/cryptoarbiter/push
mkdir /var/log/cryptoarbiter/monitoring
mkdir /var/log/cryptoarbiter/services
supervisorctl reread
supervisorctl reload
```



## Install c++ compiller

```
sudo apt install cmake
sudo apt install g++
cd ~/cryptoarbiter/calculator/rel
. run.sh
```



## Start APP

You can put all these commands at once

```
supervisorctl start wsBinance
sleep 1
supervisorctl start wsOkex
sleep 1
supervisorctl start wsHuobi
sleep 1
supervisorctl start wsBitmart
sleep 1
supervisorctl start wsBibox
sleep 1
supervisorctl start psBinance
sleep 1
supervisorctl start psOkex
sleep 1
supervisorctl start psHuobi
sleep 1
supervisorctl start psBitmart
sleep 1
supervisorctl start psBibox
sleep 1
supervisorctl start pusher
sleep 1
supervisorctl start monitor
sleep 1
supervisorctl start monitor_rebalance
sleep 1
supervisorctl start tgbot
sleep 1
echo LAUNCHED
supervisorctl status
```



## Start APP short

You can put all these commands at once

```
supervisorctl start wsBinance
sleep 1
supervisorctl start wsBibox
sleep 1
supervisorctl start psBinance
sleep 1
supervisorctl start psBibox
sleep 1
supervisorctl start pusher
sleep 1
supervisorctl start monitor
sleep 1
supervisorctl start monitor_rebalance
sleep 1
supervisorctl start tgbot
sleep 1
echo LAUNCHED
supervisorctl status
```


## Where to see results

Here *cryptoarbiter/log*




## Usefull commands

```
supervisorctl tail -5000 wsBinance stderr
supervisorctl status
cd ~/cryptoarbiter; . venv/bin/activate
```







