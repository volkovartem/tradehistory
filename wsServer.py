import json
import threading
import time

from simple_websocket_server import WebSocketServer, WebSocket

from database import select_buy_by_symbol, select_sell_by_symbol
from utils import get_preferences

pairs = get_preferences()['pairs']
symbol = ''
th1 = ''
number = 200


class SimpleEcho(WebSocket):
    def update(self):
        while True:
            if symbol != '':
                buy_trades = select_buy_by_symbol(symbol, number)
                sell_trades = select_sell_by_symbol(symbol, number)
                print('Update chart: {} {}'.format(symbol, json.dumps({
                    "buy": buy_trades,
                    "sell": sell_trades
                })))
                self.send_message(json.dumps({
                    "buy": buy_trades,
                    "sell": sell_trades
                }))
            time.sleep(1)

    def handle(self):
        global symbol
        if self.data in pairs:
            print('Select symbol: {}'.format(self.data))
            symbol = self.data.replace('_', '')

    def connected(self):
        global th1
        print(self.address, 'connected')
        th1 = threading.Thread(target=self.update)
        th1.start()

    def handle_close(self):
        print(self.address, 'closed')
        global th1
        global symbol
        th1.kill()
        symbol = ''


server = WebSocketServer('', 8000, SimpleEcho)
server.serve_forever()
