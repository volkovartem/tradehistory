import Vue from 'vue';
import App from './App.vue';
import BootstrapVue from 'bootstrap-vue';

import 'chart.js';
import 'hchs-vue-charts';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(window.VueCharts);
Vue.use(BootstrapVue);
Vue.config.productionTip = false;


new Vue({
  render: h => h(App)
}).$mount('#app');
